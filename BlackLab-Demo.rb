#!/usr/bin/env ruby

###############################################################################
#                                                                             #
#       Copyright 2018 Ryan McCoskrie <work@ryanmccoskrie.me>                 #
#                                                                             #
#       Permission is hereby granted, free of charge, to any person           #
#       obtaining a copy of this software and associated documentation        #
#       files (the "Software"), to deal in the Software without               #
#       restriction, including without limitation the rights to use,          #
#       copy, modify, merge, publish, distribute, sublicense, and/or          #
#       sell copies of the Software, and to permit persons to whom the        #
#       Software is furnished to do so, subject to the following              #
#       conditions:                                                           #
#                                                                             #
#       The above copyright notice and this permission notice shall be        #
#       included in all copies or substantial portions of the Software.       #
#                                                                             #
#       THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       #
#       EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES       #
#       OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND              #
#       NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT           #
#       HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,          #
#       WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING          #
#       FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR         #
#       OTHER DEALINGS IN THE SOFTWARE.                                       #
#                                                                             #
###############################################################################

require "twisty"
require "marginalise"
require_relative "lib/darkroom.rb"
require_relative "lib/machine.rb"
require_relative "lib/note.rb"


######################
#                    #
#      Globals       #
#                    #
######################
$engine = Twisty::Engine.instance()
$tutorialComplete = false


#######################
#                     #
#      Commands       #
#                     #
#######################
$engine.define_command(/^read \w+$/, "Read object",
	"Read a book, magazine or note") do |tokens|
	id = $engine.find_in_inventory(tokens[1])
	if $engine.items[id].class == Note
		puts $engine.items[id].text
	else
		puts "That's not a note"
	end
end

$engine.define_command(/turn on \w+$/, "Turn on machine", "Turn a machine on") do |tokens|
	$engine.current_items.each do |id|
		if $engine.items[id].name.downcase == tokens[2]
			if $engine.items[id].class == Machine
				$engine.items[id].turn_on_event
			else
				puts "You don't see a way to turn that on"
			end
		end
	end
end

$engine.define_command(/turn off \w+$/, "Turn off machine", "Turn a machine off") do |tokens|
	$engine.current_items.each do |id|
		if $engine.items[id].name.downcase == tokens[2]
			if $engine.items[id].class == Machine
				$engine.items[id].turn_off_event
			else
				puts "You don't see a way to turn that off"
			end
		end
	end
end

################################
#                              #
#       Item definitions       #
#                              #
################################
require_relative "props/props.rb"
require_relative "notes/notes.rb"

$engine.define_item(:IdCard, "ID", "A laminated card with a black and "\
	"white photograph of your face to prove you are "\
	"a janitor.".marginalise(80))

$engine.define_item(:Mop, "Mop", "Your trusty mop. A long wooden handle with "\
	"thick cotton strings at one end. Just like your grand "\
	"father used.".marginalise(80))

$engine.define_item(:Tape1, "Tape", "A real of black, magnetic tape labelled "\
	"75-03-31.")

$engine.define_item(:Bin1, "Bin", "A rectangular rubbish bin made of sturdy "\
	"lime green plastic")

################################
#                              #
#       Room definitions       #
#                              #
################################
require_relative "rooms/rooms.rb"
$engine.rooms[:LunchRoom].clone(:LunchRoomA)
$engine.rooms[:HallWay1].clone(:HallWay1A)
$engine.rooms[:HallWay1].clone(:HallWay1B)
$engine.rooms[:Office1].clone(:Office1A)
$engine.rooms[:Office1].clone(:Office1B)
$engine.rooms[:HallWay2].clone(:HallWay2A)
$engine.rooms[:HallWay2].clone(:HallWay2B)
$engine.rooms[:HallWay2].clone(:HallWay2C)
$engine.rooms[:Office2].clone(:Office2A)
$engine.rooms[:Office2].clone(:Office2B)
$engine.rooms[:BroomCloset].clone(:BroomCloset2)

############################
#                          #
#       Connections        #
#                          #
############################
$engine.give :IdCard
$engine.give :Mop

$engine.rooms[:Office1A].add_item(:Note1)
$engine.rooms[:Office2A].add_item(:Note2)

#Administrative Offices
$engine.add_door(:HallWay1A, :HallWay1B, "East")#HallWay
$engine.add_door(:HallWay1B, :HallWay1A, "West")
$engine.add_door(:HallWay1A, :TypingPool1, "West")#Typing Pool
$engine.add_door(:TypingPool1, :HallWay1A, "East")
$engine.add_door(:HallWay1A, :ConfRoom1, "North")#Conference Room
$engine.add_door(:ConfRoom1, :HallWay1A, "South")
$engine.add_door(:HallWay1A, :Office1A, "South")#Office1A
$engine.add_door(:Office1A, :HallWay1A, "North")
$engine.add_door(:HallWay1B, :Office1B, "North")#Office1B
$engine.add_door(:Office1B, :HallWay1B, "South")
$engine.add_door(:HallWay1B, :LunchRoomA, "East")#LunchRoomA
$engine.add_door(:LunchRoomA, :HallWay1B, "West")

#Programmer Offices
$engine.add_door(:HallWay2A, :HallWay2B, "East")#HallWay
$engine.add_door(:HallWay2B, :HallWay2A, "West")
$engine.add_door(:HallWay2B, :HallWay2C, "East")
$engine.add_door(:HallWay2C, :HallWay2B, "West")
$engine.add_door(:HallWay2A, :SabOffice, "West")#SabOffice
$engine.add_door(:SabOffice, :HallWay2A, "East")
$engine.add_door(:HallWay2A, :Office2A, "North")#Office2A
$engine.add_door(:Office2A, :HallWay2A, "South")
$engine.add_door(:HallWay2A, :Office2B, "South")#Office2B
$engine.add_door(:Office2B, :HallWay2A, "North")
$engine.add_door(:HallWay2B, :ConfRoom2, "North")#ConfRoom2
$engine.add_door(:ConfRoom2, :HallWay2B, "South")
$engine.add_door(:HallWay2C, :PDP8Room, "North")#PDP8Room
$engine.add_door(:PDP8Room, :HallWay2C, "South")
$engine.add_door(:HallWay2C, :BroomCloset2, "South")#BroomCloset2
$engine.add_door(:BroomCloset2, :HallWay2C, "North")
$engine.add_door(:HallWay2C, :LunchRoomB, "East")#LunchRoomB
$engine.add_door(:LunchRoomB, :HallWay2C, "West")

#Elevator
$engine.add_door(:Entrance, :Elevator, "In")#Entrance
$engine.add_door(:Elevator, :Entrance, "G")
$engine.add_door(:HallWay1A, :Elevator, "In")#HallWay1
$engine.add_door(:Elevator, :HallWay1A, "B1")
$engine.add_door(:HallWay2C, :Elevator, "In")#HallWay2
$engine.add_door(:Elevator, :HallWay2C, "B2")

#Do not allow the player to leave before finishing the tutorial
$engine.rooms[:HallWay2B].on_enter do
	if $tutorialComplete == true
		return true
	else
		puts "You need to put your bucket and mop away first."
		return false
	end
end

##########################
#                        #
#       Start Game       #
#                        #
##########################
require_relative "scenes/scenes.rb"
intro_scene()

$engine.start_loop()

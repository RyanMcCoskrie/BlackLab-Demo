###############################################################################
#                                                                             #
#       Copyright 2018 Ryan McCoskrie <work@ryanmccoskrie.me>                 #
#                                                                             #
#       Permission is hereby granted, free of charge, to any person           #
#       obtaining a copy of this software and associated documentation        #
#       files (the "Software"), to deal in the Software without               #
#       restriction, including without limitation the rights to use,          #
#       copy, modify, merge, publish, distribute, sublicense, and/or          #
#       sell copies of the Software, and to permit persons to whom the        #
#       Software is furnished to do so, subject to the following              #
#       conditions:                                                           #
#                                                                             #
#       The above copyright notice and this permission notice shall be        #
#       included in all copies or substantial portions of the Software.       #
#                                                                             #
#       THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       #
#       EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES       #
#       OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND              #
#       NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT           #
#       HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,          #
#       WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING          #
#       FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR         #
#       OTHER DEALINGS IN THE SOFTWARE.                                       #
#                                                                             #
###############################################################################

def intro_scene
	puts "In 1969 you grew bored of simply reading about robots and space "\
	"ships in magazines and made a plan to become a great "\
	"inventor.".marginalise(80)
	puts "[Press ENTER to continue]"
	gets

	puts "Six years later you have been hired by BlackLab(tm)."
	puts "[Press ENTER to continue]"
	gets

	puts "As a janitor."
	puts "[Press ENTER to continue]"
	gets

	puts "It is 11:58 PM, March 31. You have just finished cleaning the "\
	"floor in Lunch Room B while listening to the radio and it's time to "\
	"end your shift.".marginalise(80)
	puts "[Press ENTER to continue]"
	gets

	puts "Just two more months until you can afford to take a course on "\
	"electrical engineering at university.".marginalise(80)
	puts "[Type \"help\" to see the commands available]"
	puts "[Press ENTER to continue]"
	gets

	$engine.goto :LunchRoomB
end


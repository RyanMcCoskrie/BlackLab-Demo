###############################################################################
#                                                                             #
#       Copyright 2018 Ryan McCoskrie <work@ryanmccoskrie.me>                 #
#                                                                             #
#       Permission is hereby granted, free of charge, to any person           #
#       obtaining a copy of this software and associated documentation        #
#       files (the "Software"), to deal in the Software without               #
#       restriction, including without limitation the rights to use,          #
#       copy, modify, merge, publish, distribute, sublicense, and/or          #
#       sell copies of the Software, and to permit persons to whom the        #
#       Software is furnished to do so, subject to the following              #
#       conditions:                                                           #
#                                                                             #
#       The above copyright notice and this permission notice shall be        #
#       included in all copies or substantial portions of the Software.       #
#                                                                             #
#       THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       #
#       EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES       #
#       OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND              #
#       NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT           #
#       HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,          #
#       WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING          #
#       FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR         #
#       OTHER DEALINGS IN THE SOFTWARE.                                       #
#                                                                             #
###############################################################################

require "twisty"

def crisis1_scene
	$engine.goto :Entrance

	puts "Having completed work for the night you take the elevator up "\
	"to the surface level. Just as you are approaching the main door "\
	"you hear a mans voice over the intercom. \"HEY! Is anybody there? "\
	"It's an emergency!\"".marginalise(80)
	puts "[Press ENTER to continue]"
	gets

	puts "You walk over to the receptionist's desk and ask what is going "\
	"on. \"What's your security clearance?\"\nYou explain you are just a "\
	"cleaner.".marginalise(80)
	puts "[Press ENTER to continue]"
	gets

	puts "The computer on in basement two needs to be restarted. "\
		"If that isn't done by dawn worse things than you can "\
		"imagine will happen.".marginalise(80)
	puts "[Press ENTER to continue]"
	gets
	
	puts "\"But don't worry. I worked on building most of the systems "\
	"here and will guide you along every step of the way.\"".marginalise(80)
	puts "[Press ENTER to continue]"
	gets
end

###############################################################################
#                                                                             #
#       Copyright 2018 Ryan McCoskrie <work@ryanmccoskrie.me>                 #
#                                                                             #
#       Permission is hereby granted, free of charge, to any person           #
#       obtaining a copy of this software and associated documentation        #
#       files (the "Software"), to deal in the Software without               #
#       restriction, including without limitation the rights to use,          #
#       copy, modify, merge, publish, distribute, sublicense, and/or          #
#       sell copies of the Software, and to permit persons to whom the        #
#       Software is furnished to do so, subject to the following              #
#       conditions:                                                           #
#                                                                             #
#       The above copyright notice and this permission notice shall be        #
#       included in all copies or substantial portions of the Software.       #
#                                                                             #
#       THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       #
#       EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES       #
#       OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND              #
#       NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT           #
#       HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,          #
#       WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING          #
#       FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR         #
#       OTHER DEALINGS IN THE SOFTWARE.                                       #
#                                                                             #
###############################################################################

require "twisty"

$engine.define_item(:Bucket, "Bucket", "A bucket on wheels, filled with warm, "\
	"soapy water. Atop it is a pair of wooden rollers for squeezing your "\
	"mop between.".marginalise(80), :storage=>1)

bucket = $engine.items[:Bucket]

bucket.on_put_content do |id|
	if id == :Mop
		puts "You push the head of the mop between the two rollers "\
			"of the bucket so that it will stand up as you carry "\
			"both of them.".marginalise(80)
		return true
	else
		puts "You have no reason to put that in there"
		return false
	end
end

bucket.on_take do
	if bucket.contains_item?(:Mop) == true
		puts "You pick up the bucket and mop"
		return true
	else
		puts "You should put your mop in the bucket first"
		return false
	end
end

bucket.on_drop do
	if $engine.current_room.id == :BroomCloset2
		puts "You put the mop and bucket away."
		$tutorialComplete = true
		crisis1_scene()
	else
		puts "You decide against putting it down here. It belongs in "\
			"the broom closet.".marginalise(80)
	end
end

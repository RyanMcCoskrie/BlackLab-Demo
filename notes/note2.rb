###############################################################################
#                                                                             #
#       Copyright 2018 Ryan McCoskrie <work@ryanmccoskrie.me>                 #
#                                                                             #
#       Permission is hereby granted, free of charge, to any person           #
#       obtaining a copy of this software and associated documentation        #
#       files (the "Software"), to deal in the Software without               #
#       restriction, including without limitation the rights to use,          #
#       copy, modify, merge, publish, distribute, sublicense, and/or          #
#       sell copies of the Software, and to permit persons to whom the        #
#       Software is furnished to do so, subject to the following              #
#       conditions:                                                           #
#                                                                             #
#       The above copyright notice and this permission notice shall be        #
#       included in all copies or substantial portions of the Software.       #
#                                                                             #
#       THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       #
#       EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES       #
#       OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND              #
#       NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT           #
#       HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,          #
#       WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING          #
#       FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR         #
#       OTHER DEALINGS IN THE SOFTWARE.                                       #
#                                                                             #
###############################################################################
$engine.define_note(:Note2, "Note2", "A piece of grid paper with a few "\
	"words scrawled at the top followed by a pattern of "\
	"ones and zeros".marginalise(80),
		"RIM - down is one, leave the first six up\n\n"\
		"111111101110110000011010\n"\
		"111111101111100000011001\n"\
		"111111110000101011101111\n"\
		"111111110001110000011110\n"\
		"111111110010111001000110\n"\
		"111111110011111000000110\n"\
		"111111110100111101001000\n"\
		"111111110101101011101111\n"\
		"111111110110111000000110\n"\
		"111111110111110000011001\n"\
		"111111111000101011110111\n"\
		"111111111001110000011100\n"\
		"111111111010111100010000\n"\
		"111111111011011111111110\n"\
		"111111111100011011111110\n"\
		"111111111101101011101110")

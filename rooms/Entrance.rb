###############################################################################
#                                                                             #
#       Copyright 2018 Ryan McCoskrie <work@ryanmccoskrie.me>                 #
#                                                                             #
#       Permission is hereby granted, free of charge, to any person           #
#       obtaining a copy of this software and associated documentation        #
#       files (the "Software"), to deal in the Software without               #
#       restriction, including without limitation the rights to use,          #
#       copy, modify, merge, publish, distribute, sublicense, and/or          #
#       sell copies of the Software, and to permit persons to whom the        #
#       Software is furnished to do so, subject to the following              #
#       conditions:                                                           #
#                                                                             #
#       The above copyright notice and this permission notice shall be        #
#       included in all copies or substantial portions of the Software.       #
#                                                                             #
#       THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       #
#       EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES       #
#       OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND              #
#       NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT           #
#       HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,          #
#       WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING          #
#       FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR         #
#       OTHER DEALINGS IN THE SOFTWARE.                                       #
#                                                                             #
###############################################################################
require_relative "../lib/machine.rb"

room = $engine.define_room(:Entrance, "Entrance", "The entrance to "\
	"BlackLab(tm).\nThe walls are bright orange, the floor checkered "\
	"and the ceiling is made of slabs with strange little holes "\
	"in them.".marginalise(80))

$engine.define_item(:RecpDesk, "Desk", "The reception desk of BlackLab(tm). "\
	"The chances of a non-employee visiting the island are close to zero "\
	"but there is a need to coordinate everyone coming to work in the "\
	"morning. The front side is covered in dark wood laid out in a "\
	"diagonal pattern that goes just high enough over the work surface "\
	"to look intimidating.".marginalise(80),
	:fixed => true, :storage => 5)

intercom = $engine.define_machine(:RecpInterCom, "InterCom", "A loud "\
	"speaker, microphone and switches for every department of "\
	"BlackLab(tm) on top of the reception desk.".marginalise(80))

room.add_item(:RecpDesk)
room.add_item(:RecpInterCom)

$engine.items[:RecpDesk].add_item(:Torch)

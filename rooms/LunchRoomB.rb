###############################################################################
#                                                                             #
#       Copyright 2018 Ryan McCoskrie <work@ryanmccoskrie.me>                 #
#                                                                             #
#       Permission is hereby granted, free of charge, to any person           #
#       obtaining a copy of this software and associated documentation        #
#       files (the "Software"), to deal in the Software without               #
#       restriction, including without limitation the rights to use,          #
#       copy, modify, merge, publish, distribute, sublicense, and/or          #
#       sell copies of the Software, and to permit persons to whom the        #
#       Software is furnished to do so, subject to the following              #
#       conditions:                                                           #
#                                                                             #
#       The above copyright notice and this permission notice shall be        #
#       included in all copies or substantial portions of the Software.       #
#                                                                             #
#       THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       #
#       EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES       #
#       OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND              #
#       NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT           #
#       HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,          #
#       WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING          #
#       FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR         #
#       OTHER DEALINGS IN THE SOFTWARE.                                       #
#                                                                             #
###############################################################################

room = $engine.rooms[:LunchRoom].clone(:LunchRoomB)

room.on_exit do
	if $engine.inventory.include?(:Bucket) and
		$engine.items[:Bucket].contains_item?(:Mop)
		if $engine.items[:Radio1].on == false
			return true
		else
			puts "You can't leave until you turn the radio off"
			return false
		end
	else
		puts "You remember you need to put your bucket and mop away"
		return false
	end
end

room.on_enter do
	if $tutorialComplete == true
		puts "You have no more business there."
		return false
	else
		return true
	end
end

radio = $engine.define_machine(:Radio1, "Radio", "A radio left over from "\
	"the war that somebody keeps on a table in the corner. One of the "\
	"few luxuries provided by BlackLab(tm) is a radio station on the "\
	"island.".marginalise(80), :on => true)
radio = $engine.items[:Radio1]

radio.on_turn_on do
	if @on == false
		puts "Music slowly fades in as the valves warm up. The "\
		"islands' single station is playing \"Set the controls for "\
		"the heart of the sun\" by Pink Floyd.".marginalise(80)
		@on = true
	else
		puts "The #{@name} is already on"
	end
end

radio.on_turn_off do
	if @on == true
		puts "With a click of the dial the music fades away."
		@on = false
	else
		puts "The #{@name} is already off"
	end
end

room.add_item(:Radio1)
room.add_item(:Bucket)


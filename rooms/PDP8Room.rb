###############################################################################
#                                                                             #
#       Copyright 2018 Ryan McCoskrie <work@ryanmccoskrie.me>                 #
#                                                                             #
#       Permission is hereby granted, free of charge, to any person           #
#       obtaining a copy of this software and associated documentation        #
#       files (the "Software"), to deal in the Software without               #
#       restriction, including without limitation the rights to use,          #
#       copy, modify, merge, publish, distribute, sublicense, and/or          #
#       sell copies of the Software, and to permit persons to whom the        #
#       Software is furnished to do so, subject to the following              #
#       conditions:                                                           #
#                                                                             #
#       The above copyright notice and this permission notice shall be        #
#       included in all copies or substantial portions of the Software.       #
#                                                                             #
#       THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       #
#       EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES       #
#       OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND              #
#       NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT           #
#       HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,          #
#       WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING          #
#       FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR         #
#       OTHER DEALINGS IN THE SOFTWARE.                                       #
#                                                                             #
###############################################################################
room = $engine.define_room(:PDP8Room, "PDP8 Room", "Unlike the offices above "\
	"the walls are bare, unpainted wood decorated with the occasional "\
	"doodle.".marginalise(80))

room.on_enter do
	if $tutorialComplete == true
		return true
	else
		puts "You need to put away your mop and bucket first."
		return false
	end
end

pdp = $engine.define_machine(:PDP8, "PDP8", "A Digital PDP-8/I computer. The "\
	"front panel is covered in words cryptic words like \"Opr\", along "\
	"the bottom are two key holes and twenty-six two-position switches. "\
	"A large section on the left of the front panel has been covered by "\
	"green paint.".marginalise(80))
room.add_item(:PDP8)

pdp.on_turn_on do
	if $engine.inventory.include? :PDP8Key
		puts "You insert and turn the key."
		if $engine.inventory.include? :Note2
			puts "You carefully toggle the switches to match "\
				"the first line of ones and zeros, push "\
				"execute and then repeat for the next "\
				"fifteen lines. When you reach the last line "\
				"the computer starts happily running on its "\
				"own.".marginalise(80)

			puts "\n\n\n"
			puts "Congratulations on completing finishing the "\
				"demo. I promise the finished game will be "\
				"far longer, harder and have a proper "\
				"story.".marginalise(80)
			engine.end_loop

			return true
		else
			puts "The computer sits waiting for instructions."
			puts "Reluctantly you turn it back off."
			return false
		end
	else
		puts "You can not start the computer without its key."
		return false
	end
end

pdp.on_turn_off do
	puts "That doesn't seem like a good idea."
	return false
end

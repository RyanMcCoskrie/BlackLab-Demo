###############################################################################
#                                                                             #
#       Copyright 2018 Ryan McCoskrie <work@ryanmccoskrie.me>                 #
#                                                                             #
#       Permission is hereby granted, free of charge, to any person           #
#       obtaining a copy of this software and associated documentation        #
#       files (the "Software"), to deal in the Software without               #
#       restriction, including without limitation the rights to use,          #
#       copy, modify, merge, publish, distribute, sublicense, and/or          #
#       sell copies of the Software, and to permit persons to whom the        #
#       Software is furnished to do so, subject to the following              #
#       conditions:                                                           #
#                                                                             #
#       The above copyright notice and this permission notice shall be        #
#       included in all copies or substantial portions of the Software.       #
#                                                                             #
#       THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       #
#       EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES       #
#       OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND              #
#       NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT           #
#       HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,          #
#       WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING          #
#       FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR         #
#       OTHER DEALINGS IN THE SOFTWARE.                                       #
#                                                                             #
###############################################################################
room = $engine.define_room(:ConfRoom2, "Conference Room", "A room where "\
	"leading computer scientists can plan projects together. Unlike its "\
	"counterpart in the administrative level above there are no "\
	"paintings, the lighting is fixed to bright white and the walls "\
	"are an energetic yellow.".marginalise(80))

item = $engine.define_item(:ConfRoom2Board, "Board", "A reversable black "\
	"board with a microchip block diagram on it.".marginalise(80),
	:fixed => true)

room.add_item(:ConfRoom2Board)
room.add_item(:ConfRoomTable)
room.add_item(:ConfRoomChairs)

###############################################################################
#                                                                             #
#       Copyright 2018 Ryan McCoskrie <work@ryanmccoskrie.me>                 #
#                                                                             #
#       Permission is hereby granted, free of charge, to any person           #
#       obtaining a copy of this software and associated documentation        #
#       files (the "Software"), to deal in the Software without               #
#       restriction, including without limitation the rights to use,          #
#       copy, modify, merge, publish, distribute, sublicense, and/or          #
#       sell copies of the Software, and to permit persons to whom the        #
#       Software is furnished to do so, subject to the following              #
#       conditions:                                                           #
#                                                                             #
#       The above copyright notice and this permission notice shall be        #
#       included in all copies or substantial portions of the Software.       #
#                                                                             #
#       THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       #
#       EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES       #
#       OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND              #
#       NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT           #
#       HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,          #
#       WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING          #
#       FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR         #
#       OTHER DEALINGS IN THE SOFTWARE.                                       #
#                                                                             #
###############################################################################

class Machine < Twisty::Item
	attr_accessor :on

	def initialize(id, name, desc, fixed, storage, on)
		@id = id
		@name = name
		@desc = desc
		@fixed = fixed
		@storage = storage
		if @storage > 0
			@contents = []
		end
		@on = on
	end

	rtype [Symbol] => Machine
	def clone(other)
		return engine.define_machine(other, @name, @desc,
			:fixed => @true, :storage => @storage, :on => @on)
	end

	rtype [] => nil
	def turn_on_event
		if @on == true
			puts "The #{@name} is already on"
		else
			puts "Nothing happens"
		end

		return nil
	end

	rtype [] => nil
	def turn_off_event
		if @on == false
			puts "The #{@name} is already off"
		else
			puts "Nothing happens"
		end

		return nil
	end

	rtype [] => nil
	def turn_off_event
		puts "Nothing happens"
		return nil
	end

	rtype [Proc] => nil
	def on_turn_on(&block)
		self.define_singleton_method(:turn_on_event, &block)
		return nil
	end

	rtype [Proc] => nil
	def on_turn_off(&block)
		self.define_singleton_method(:turn_off_event, &block)
		return nil
	end
end

module Twisty
class Engine
	rtype [Symbol, String, String, Hash] => Machine
	def define_machine(id, name, desc, options={})
		options = {fixed: true, storage: 0, on: false}.merge(options)
		fixed = options[:fixed]
		storage = options[:storage]
		on = options[:on]

		if @items[id] == nil
			@items[id] = Machine.new(id, name, desc, fixed, storage, on)
		else
			raise GameError.new "Item #{id} has already been defined"
		end

		return @items[id]
	end
end
end
